package ucsal.todo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

    private List<Item> itens;
    private Context context;

    public Adapter(Context context, List<Item> itens){
        this.context = context;
        this.itens = itens;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        //TODO: IMPLEMENAR O BIND
        holder.name.setText(itens.get(position).getText());
        holder.description.setText(itens.get(position).getText());
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private TextView description;

        private Button botao;
        public Holder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_text_title);
            description = itemView.findViewById(R.id.item_text_text);
            botao = itemView.findViewById(R.id.item_button_finish);
            botao.setOnClickListener(this);
        }


        private void setName(String name){
            this.name.setText(name);
        }

        private void setDescription(String description){
            this.description.setText(description);
        }


        @Override
        public void onClick(View v) {
            int position = this.getAdapterPosition();
            Item model = itens.get(position);
            itens.remove(model);
            notifyItemRemoved(position);
        }
    }

}
