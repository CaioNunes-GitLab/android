package com.example.recicleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Trabalho> trabalhos = new ArrayList<>();
        trabalhos.add(new Trabalho("Titulo1","subtitulo1"));
        trabalhos.add(new Trabalho("Titulo2","subtitulo2"));
        trabalhos.add(new Trabalho("Titulo3","subtitulo3"));

        RecyclerView recyclerView = findViewById(R.id.main_recycleview_lista);

        Adapter adapter = new Adapter(this,trabalhos);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}