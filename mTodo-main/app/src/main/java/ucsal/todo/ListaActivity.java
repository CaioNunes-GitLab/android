package ucsal.todo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ListaActivity extends AppCompatActivity {

    private FloatingActionButton novo;
    private RecyclerView lista;

    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        lista = findViewById(R.id.lista_recycle_atividades);
        adapter = new Adapter(this, Dados.getItens() );
        lista.setAdapter(adapter);

        lista.setLayoutManager(new LinearLayoutManager(this));

        novo = findViewById(R.id.lista_fab_novo);
        novo.setOnClickListener(v -> {
            Intent intent = new Intent(ListaActivity.this, FormActivity.class);
            startActivity(intent);
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }
}