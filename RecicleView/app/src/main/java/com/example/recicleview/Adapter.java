package com.example.recicleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.zip.Inflater;

public class Adapter extends RecyclerView.Adapter implements View.OnClickListener {

    private List lista;
    private Inflater inflater;

    public Adapter(Context context, List lista) {
        this.lista = lista;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titulo;
        private TextView subtitulo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.item_titulo);
            titulo = itemView.findViewById(R.id.item_subtitulo);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_lista_main, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Trabalho trabalho = this.lista.get(position);

    }

    @Override
    public int getItemCount() {
        return this.lista.size();
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), );
    }
}
