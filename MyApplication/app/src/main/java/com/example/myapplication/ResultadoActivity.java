package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ResultadoActivity extends AppCompatActivity {

    private TextView label;
    private FloatingActionButton fabButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        String valor = getIntent().getStringExtra("Texto_Digitado");
        Log.d("ResultadoActivity", "Texto que chegou: " + valor);

        fabButton = findViewById(R.id.resultado_fab_voltar);
        label = findViewById(R.id.texto_resultado);

        label.setText(valor);

        fabButton.setOnClickListener(view -> {
            Intent intent = new Intent(ResultadoActivity.this, MainActivity.class);
            startActivity(intent);
        });

    }
}