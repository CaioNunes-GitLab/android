package ucsal.todo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class Dados {

    public static List<Item> itens = new ArrayList<Item>();
    public static AtomicLong index = new AtomicLong();


    public static void addItem(Item item){
        item.setId(index.getAndIncrement());
        itens.add(item);
    }

    public static List<Item> getItens() {
        return itens;
    }
}



