package ucsal.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputEditText;

public class FormActivity extends AppCompatActivity {


    private EditText name;
    private EditText description;

    private Button salvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        name = findViewById(R.id.form_field_title);
        description = findViewById(R.id.form_field_text);

        salvar = findViewById(R.id.form_button_salvar);

        salvar.setOnClickListener(v -> {
            Item item = new Item();
            item.setTitle(name.getText().toString());
            item.setText(description.getText().toString());
            Dados.addItem(item);
            finish();
            //TODO: VOLTAR
        });

    }


}